export const mutations = {
  addItem(state, payload) {
    state.tasks = [...state.tasks, payload];
  },
  deleteItem(state, payload) {
    let { tasks } = state;

    state.tasks = tasks.filter(({ id }) => {
      return id != payload;
    });
  },
  updateItem(state, payload) {
    let { tasks } = state;
    let task = null;
    let Remaingtask = null;
    task = tasks.filter(({ id }) => {
      return id == payload.id;
    });
    Remaingtask = tasks.filter(({ id }) => {
      return id != payload / id;
    });

    task.isCompletd = !payload.isCompletd;

    state.tasks = [...Remaingtask, task];
  },
};
