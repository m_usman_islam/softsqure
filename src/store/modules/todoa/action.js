export const actions = {
  addItem({ commit }, payload) {
    commit('addItem', payload);
  },
  deleteItem({ commit }, payload) {
    commit('deleteItem', payload);
  },
  updateItem({ commit }, payload) {
      
    commit('updateItem', payload);
  },
};
