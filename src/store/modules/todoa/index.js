import { actions } from './action';
import { mutations } from './mutation';
import { getters } from './gettters';

import { reactive } from 'vue';

import { useLocalStorage } from '@vueuse/core';
const state = reactive(
  useLocalStorage('todoapp', {
    tasks: [
      {
        id: 1,
        name: 'Bur Food',
        descrtiopn: 'lorem ipsu',
        isCompletd: false,
      },
      {
        id: 2,
        name: 'Bur Food',
        descrtiopn: 'lorem ipsu',
        isCompletd: true,
      },
      {
        id: 3,
        name: 'Bur Food',
        descrtiopn: 'lorem ipsu',
        isCompletd: true,
      },
    ],
  })
);

const todo = {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
};
export default todo;
