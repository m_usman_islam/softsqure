import { createStore } from 'vuex';
import TodoApp from './modules/todoa/index';
export default createStore({
  modules: {
    TodoApp,
  },
});
